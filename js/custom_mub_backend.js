$(document).ready(function(){
	$('#mub-state').on('change',function(){
    var selected = $("#mub-state option:selected").val();
    $.ajax({
         type:'POST',
         url:"/mub-admin/users/user/get-city",
         data:{stateId:selected},
        success: function(result){
            $('#mubusercontact-city').children("option").remove();
            $('#mubusercontact-city').prop("disabled", false);
            $('#mubusercontact-city').append(new Option('Select', ''));
        $.each(result.result, function (i, item) {
            $('#mubusercontact-city').append(new Option(item.city_name, item.id));
        });}
    }); 
 });
});