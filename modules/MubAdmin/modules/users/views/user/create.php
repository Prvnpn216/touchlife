<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MubUser */

$this->title = Yii::t('app', 'Create Mub User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mub Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mub-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'mubUser' => $mubUser,
        'mubUserContacts' => $mubUserContacts,
        'allStates' => $allStates
    ]) ?>

</div>