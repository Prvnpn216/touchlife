<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\City;
if(Yii::$app->controller->action->id == 'update')
{
    $city = $mubUserContacts->city;
    $cityModel = new City();
    $state = $cityModel::findOne($city)->state_id;
    $allCities = $cityModel->getManyById($state,'state_id','city_name');
}
?>

<div class="mub-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($mubUserContacts, 'email')->textInput();?>

    <?php 
    if(Yii::$app->controller->action->id == 'update')
{?>
    <?php echo  Html::dropDownList('mub-state',null,$allStates,['id' => 'mub-state','class'=>'form-control','options' => [$state => ['Selected' => true]]]);?>
   
    <?= $form->field($mubUserContacts, 'city')->dropDownList($allCities, ['prompt' => 'Select A City']);?>
<?php }
else
{ ?>
<?php echo  Html::dropDownList('mub-state',null,$allStates,['id' => 'mub-state','class'=>'form-control']);?>
   
    <?= $form->field($mubUserContacts, 'city')->dropDownList([], ['prompt' => 'Select A City']);?>
 <?php }
    ?>


    <?= $form->field($mubUser, 'domain')->textInput(['maxlength' => true])->label('Website url/name'); ?>
    
    <?= $form->field($mubUser, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($mubUser, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($mubUser, 'gender')->dropDownList([ 'Male' => 'Male', 'Female' => 'Female', 'Other' => 'Other', ], ['prompt' => '']) ?>

    <?= $form->field($mubUser, 'dob')->textInput() ?>


    <?= $form->field($mubUser, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($mubUser, 'password')->passwordInput(['maxlength' => true]);?>

    <?= $form->field($mubUser, 'organization')->textInput(['maxlength' => true]) ?>

    <?= $form->field($mubUser, 'status')->dropDownList([ 'Active' => 'Active', 'Inactive' => 'Inactive', ], ['prompt' => 'Select A Status']) ?>

    <div class="form-group">
        <?= Html::submitButton($mubUser->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $mubUser->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>