<?php

namespace app\modules\MubAdmin\modules\users\controllers;

use Yii;
use app\models\MubUser;
use app\models\MubUserSearch;
use app\modules\MubAdmin\modules\users\models\UserProcess;
use app\models\City;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class UserController extends MubController
{
   
	public function getProcessModel()
    {
        return new UserProcess();
    }

    public function getPrimaryModel()
    {
        return new MubUser();
    }

    public function getSearchModel()
    {
        return new MubUserSearch();
    }

    public function actionGetCity() {
        if (Yii::$app->request->isAjax) {
            $stateId = Yii::$app->request->post('stateId');
            $result = City::find()->where(['state_id' => $stateId])->all();
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'result' => $result,
            ];
        } else {
            return false;
        }
    }

}