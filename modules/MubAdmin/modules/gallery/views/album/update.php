<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MubUserAlbum */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Mub User Album',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mub User Albums'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="mub-user-album-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>