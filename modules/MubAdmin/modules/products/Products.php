<?php

namespace app\modules\MubAdmin\modules\products;

class Products extends \app\modules\MubAdmin\MubAdmin
{
    public $controllerNamespace = 'app\modules\MubAdmin\modules\products\controllers';

    public $defaultRoute = 'album';

    public function init()
    {
        parent::init();

    }
}
