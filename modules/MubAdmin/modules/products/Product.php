<?php

namespace app\modules\MubAdmin\modules\products;

/**
 * products module definition class
 */
class Product extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\MubAdmin\modules\products\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
