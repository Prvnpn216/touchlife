<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/style.css',
        'css/touchTouch.css',
        'css/swipebox.css',
    ];
    public $js = [
        'js/custom_mub_frontend.js',
	    'js/custom_mub_backend.js',
	    'js/responsiveslides.min.js',
        'js/bootstrap.js',
        'js/easing.js',
        'js/move-top.js',
        'js/jquery.fancybox.js',
        'js/jquery.swipebox.min.js',
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\assets\FontAwesomeAsset'
    ];
}
