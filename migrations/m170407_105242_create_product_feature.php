<?php

namespace app\migrations;
use app\commands\Migration;

class m170407_105242_create_product_feature extends Migration
{
    public function getTableName()
    {
        return 'product_feature';
    }
    
    public function getForeignKeyFields()
    {
        return [
            'product_id' => ['product', 'id'],
            'mub_user_id' => ['mub_user','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'feature' => 'feature',
            'del_status' => 'del_status'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->defaultValue(NULL),
            'mub_user_id' => $this->integer()->notNull(), 
            'feature' => $this->string(50)->notNull(),
            'description' => $this->text(),
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
