<?php

namespace app\migrations;
use app\commands\Migration;

class m170115_144641_create_mub_social_platforms extends Migration
{
   public function getTableName()
    {
        return 'mub_social_platforms';
    }

    public function getKeyFields()
    {
        return [
            'domain' => 'domain',
            'name' => 'name',
            'slug'  =>  'slug',
        ];
    }

    public function getFields()
    {
        return [
        'id' => $this->primaryKey(),
        'name' => $this->string(100)->notNull(),
        'domain' => $this->string(100)->notNull(),
        'slug' => $this->string(100)->notNull(),
        'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
