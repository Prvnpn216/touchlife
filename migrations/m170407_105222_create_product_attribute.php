<?php

namespace app\migrations;
use app\commands\Migration;

class m170407_105222_create_product_attribute extends Migration
{
     public function getTableName()
    {
        return 'product_attribute';
    }
    public function getForeignKeyFields()
    {
        return [
            'category_id' => ['product_category', 'id'],
            'mub_user_id' => ['mub_user','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'slug' => 'slug',
            'status' => 'status'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->defaultValue(NULL),
            'category_id' => $this->integer()->notNull(),
            'title' => $this->string(50)->notNull(),
            'description' => $this->string(100)->notNull(),
            'slug' => $this->string(),
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
