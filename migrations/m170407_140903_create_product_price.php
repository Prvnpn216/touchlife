<?php
namespace app\migrations;
use app\commands\Migration;

class m170407_140903_create_product_price extends Migration
{
    public function getTableName()
    {
        return 'product_price';
    }
    
    public function getForeignKeyFields()
    {
        return [
            'product_id' => ['product', 'id'],
            'attribute_id' => ['product_attribute','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'price' => 'price',
        ];
    }

    public function getFields()
    {
        //if product_id is null price is for attribute and vise versa 
        return [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->defaultValue(NULL),
            'attribute_id' => $this->integer()->defaultValue(NULL),
            'price' => $this->double(2)->notNull(),
            'base_price' => $this->double(2),
            'display' => "enum('0','1') NOT NULL DEFAULT '1'",
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
