<?php

namespace app\migrations;
use app\commands\Migration;

class m170117_114054_create_post_detail extends Migration
{
    public function getTableName()
    {
        return 'post_detail';
    }
    public function getForeignKeyFields()
    {
        return [
            'post_id' => ['post','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'post_id' => 'post_id',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer()->defaultValue(NULL),
            'post_content' => "text NOT NULL",
            'updated_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
