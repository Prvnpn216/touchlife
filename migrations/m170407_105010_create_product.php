<?php

namespace app\migrations;
use app\commands\Migration;

class m170407_105010_create_product extends Migration
{
    public function getTableName()
    {
        return 'product';
    }
    public function getForeignKeyFields()
    {
        return [
            'category_id' => ['product_category', 'id'],
            'mub_user_id' => ['mub_user','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'name' => 'name',
            'slug' => 'slug'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->defaultValue(NULL),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'status' => "enum('0','1') COMMENT '0 => Inactive, 1 => Active'",
            'description' => "text",
            'category_id' => $this->integer()->defaultValue(NULL),
            'pinned' => "enum('0','1')",
            'order' => $this->integer()->notNull()->defaultValue('9999'),
            'approved_by' => $this->integer(),           
            'approved_on' => $this->dateTime(),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
