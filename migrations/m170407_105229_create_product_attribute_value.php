<?php

namespace app\migrations;
use app\commands\Migration;

class m170407_105229_create_product_attribute_value extends Migration
{
    public function getTableName()
    {
        return 'product_attribute_value';
    }
    
    public function getForeignKeyFields()
    {
        return [
            'attribute_id' => ['product_attribute', 'id'],
            'mub_user_id' => ['mub_user','id'],
            'product_id' => ['product','id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'value' => 'value',
            'mub_user_id' => 'mub_user_id',
            'del_status' => 'del_status'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->defaultValue(NULL),
            'product_id' => $this->integer()->notNull(),
            'attribute_id' => $this->integer()->notNull(),
            'value' => $this->string()->notNull(),
            'description' => $this->string(100)->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
