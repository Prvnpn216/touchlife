<?php

namespace app\migrations;
use app\commands\Migration;

class m170117_115022_create_mub_elements extends Migration
{
    public function getTableName()
    {
        return 'mub_element';
    }

    public function getKeyFields()
    {
        return [
            'name' => 'name',
            'title'  =>  'title',
            'order' => 'order'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'title' => $this->string(100),
            'type' => $this->string(100),
            'tag' => $this->string(100),
            'parent_tag' => $this->string(100),
            'parent_class' => $this->string(100),
            'class' => $this->string(100),
            'order' => $this->integer(100),
            'x_start' => $this->string(100),
            'y_start' => $this->string(100),
            'x_end' => $this->string(100),
            'y_end' => $this->string(100),
            'created_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime()->notNull()->defaultValue('1970-01-01 12:00:00'),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
