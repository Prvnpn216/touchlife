<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_feature".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $mub_user_id
 * @property string $feature
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property MubUser $mubUser
 * @property Product $product
 */
class ProductFeature extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_feature';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'mub_user_id'], 'integer'],
            [['mub_user_id', 'feature'], 'required'],
            [['description', 'status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['feature'], 'string', 'max' => 50],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'mub_user_id' => 'Mub User ID',
            'feature' => 'Feature',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
