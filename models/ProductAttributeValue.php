<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_attribute_value".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property integer $attribute_id
 * @property string $value
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property ProductAttribute $attribute
 * @property MubUser $mubUser
 */
class ProductAttributeValue extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_attribute_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'attribute_id'], 'integer'],
            [['attribute_id', 'value', 'description'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['status', 'del_status'], 'string'],
            [['value'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 100],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductAttribute::className(), 'targetAttribute' => ['attribute_id' => 'id']],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'attribute_id' => 'Attribute ID',
            'value' => 'Value',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttribute()
    {
        return $this->hasOne(ProductAttribute::className(), ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }
}
