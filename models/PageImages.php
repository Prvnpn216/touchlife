<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "page_images".
 *
 * @property integer $id
 * @property integer $page_id
 * @property integer $image_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUserImages $image
 * @property MubUserPage $page
 */
class PageImages extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'image_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['del_status'], 'string'],
            [['image_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUserImages::className(), 'targetAttribute' => ['image_id' => 'id']],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUserPage::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'page_id' => Yii::t('app', 'Page ID'),
            'image_id' => Yii::t('app', 'Image ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(MubUserImages::className(), ['id' => 'image_id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(MubUserPage::className(), ['id' => 'page_id'])->where(['del_status' => '0']);
    }
}
