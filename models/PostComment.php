<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post_comment".
 *
 * @property integer $id
 * @property integer $post_id
 * @property integer $page_id
 * @property string $comment_text
 * @property integer $mub_user_id
 * @property string $status
 * @property string $approved_on
 * @property integer $approved_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $approvedBy
 * @property MubUser $mubUser
 * @property MubUserPage $page
 * @property Post $post
 */
class PostComment extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'page_id', 'mub_user_id', 'approved_by'], 'integer'],
            [['comment_text', 'status', 'del_status'], 'string'],
            [['mub_user_id'], 'required'],
            [['approved_on', 'created_at', 'updated_at'], 'safe'],
            [['approved_by'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['approved_by' => 'id']],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUserPage::className(), 'targetAttribute' => ['page_id' => 'id']],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Post::className(), 'targetAttribute' => ['post_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'post_id' => Yii::t('app', 'Post ID'),
            'page_id' => Yii::t('app', 'Page ID'),
            'comment_text' => Yii::t('app', 'Comment Text'),
            'mub_user_id' => Yii::t('app', 'Mub User ID'),
            'status' => Yii::t('app', 'Status'),
            'approved_on' => Yii::t('app', 'Approved On'),
            'approved_by' => Yii::t('app', 'Approved By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'approved_by'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(MubUserPage::className(), ['id' => 'page_id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id'])->where(['del_status' => '0']);
    }
}
