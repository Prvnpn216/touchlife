<?php 

namespace app\models;
use app\components\Model;
use app\yii2mod\cart\models\CartItemInterface;

class ProductModel extends Model implements CartItemInterface
{
    public function getPrice()
    {
        return $this->price;
    }

    public function getLabel()
    {
        return $this->name;
    }

    public function getUniqueId()
    {
        return $this->id;
    }
}