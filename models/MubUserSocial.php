<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mub_user_social".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property integer $social_platform_id
 * @property string $platform_username
 * @property string $url
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property string $del_status
 *
 * @property MubUser $mubUser
 * @property MubSocialPlatforms $socialPlatform
 */
class MubUserSocial extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mub_user_social';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'social_platform_id'], 'integer'],
            [['social_platform_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['status', 'del_status'], 'string'],
            [['platform_username', 'url', 'type'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['social_platform_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubSocialPlatforms::className(), 'targetAttribute' => ['social_platform_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'mub_user_id' => Yii::t('app', 'Mub User ID'),
            'social_platform_id' => Yii::t('app', 'Social Platform ID'),
            'platform_username' => Yii::t('app', 'Platform Username'),
            'url' => Yii::t('app', 'Url'),
            'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
            'del_status' => Yii::t('app', 'Del Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialPlatform()
    {
        return $this->hasOne(MubSocialPlatforms::className(), ['id' => 'social_platform_id'])->where(['del_status' => '0']);
    }
}
