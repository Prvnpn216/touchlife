<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property string $name
 * @property string $slug
 * @property string $status
 * @property string $description
 * @property integer $category_id
 * @property string $pinned
 * @property integer $order
 * @property integer $approved_by
 * @property string $approved_on
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property ProductCategory $category
 * @property MubUser $mubUser
 * @property ProductFeature[] $productFeatures
 * @property ProductImages[] $productImages
 * @property ProductPrice[] $productPrices
 */
class Product extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'category_id', 'order', 'approved_by'], 'integer'],
            [['name', 'slug'], 'required'],
            [['status', 'description', 'pinned', 'del_status'], 'string'],
            [['approved_on', 'created_at', 'updated_at'], 'safe'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'status' => 'Status',
            'description' => 'Description',
            'category_id' => 'Category ID',
            'pinned' => 'Pinned',
            'order' => 'Order',
            'approved_by' => 'Approved By',
            'approved_on' => 'Approved On',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductFeatures()
    {
        return $this->hasMany(ProductFeature::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductImages()
    {
        return $this->hasMany(ProductImages::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPrices()
    {
        return $this->hasMany(ProductPrice::className(), ['product_id' => 'id']);
    }
}
