<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Register';
?>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Sign Up <small> Form</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <?php $form = ActiveForm::begin([
              'id' => 'login-form',
              'class' => 'form-horizontal form-label-left',
              ]); ?>
          <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <?= $form->field($model, 'first_name')->textInput(['autofocus' => true,'class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          <div class="form-group col-md-6 col-sm-6 col-xs-12">
              <?= $form->field($model, 'last_name')->textInput(['class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          <div class="form-group col-md-6 col-sm-6 col-xs-12">
              <?= $form->field($model, 'organization')->textInput(['class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          <div class="form-group col-md-6 col-sm-6 col-xs-12">
              <?= $form->field($model, 'domain')->textInput(['class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          <div class="form-group col-md-6 col-sm-6 col-xs-12">
              <?= $form->field($model, 'username')->textInput(['class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          <div class="form-group col-md-6 col-sm-6 col-xs-12">
              <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          <div class="form-group col-md-6 col-sm-6 col-xs-12">
              <?= $form->field($model, 'password_confirm')->passwordInput(['class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          <div class="form-group col-md-6 col-sm-6 col-xs-12">
              <?= $form->field($model, 'email')->textInput(['class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          <div class="form-group col-md-6 col-sm-6 col-xs-12">
              <?= $form->field($model, 'mobile')->textInput(['class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          <div class="form-group col-md-6 col-sm-6 col-xs-12">
              <?= $form->field($model, 'address')->textInput(['class' => 'form-control col-md-7 col-xs-12']);?>
          </div>
          <div class="form-group col-md-6 col-sm-6 col-xs-12">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender <span class="required">*</span></label>
              <div id="gender" class="btn-group" data-toggle="buttons">
                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <?= $form->field($model, 'gender')->radio(['label' => 'Male', 'value' => 'Male', 'uncheck' => null]) ?>
                </label>
                <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                   <?= $form->field($model, 'gender')->radio(['label' => 'Female', 'value' => 'Female', 'uncheck' => null]) ?>
                </label>
              </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="reset" class="btn btn-primary">Cancel</button>
              <button type="submit" class="btn btn-success">Submit</button>
            </div>
          </div>
         <?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>
</div>