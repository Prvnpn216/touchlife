<?php 

$menu = [];
if(\Yii::$app->user->can('dashboard/index'))
{
    $menu[] = ["label" => "Dashboard", "url" => "/mub-admin", "icon" => "home"];        
}
if(\Yii::$app->user->can('album/index'))
{
$menu[] = [
            "label" => "Gallery", 
            "url" => ["/mub-admin/gallery"], 
            "icon" => "picture-o"
        ];
}
if(\Yii::$app->user->can('user/index'))
{
$menu[] = ["label" => "Users", "url" => ["/mub-admin/users"], "icon" => "user"];
}
if(\Yii::$app->user->can('products/index'))
{
$menu[] = [
            "label" => "Products",
            "icon" => "bold",
            "url" => ["/mub-admin/blog"],
        ];
}